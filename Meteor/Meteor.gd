extends Area2D

var pMeteorEffect := preload("res://Meteor/MeteorEffect.tscn")

export var minSpeed: float = 10
export var maxSpeed: float = 20
export var minRotationRate: float = -20
export var maxRotationRate: float = 20

export var life: int = 20

var speed: float = 0
var rotationRate: float = 0
var playerInArea: Player = null

func _ready():
	speed = rand_range(minSpeed, maxSpeed)
	rotationRate = rand_range(minRotationRate, maxRotationRate)
	
# Keep damaging the Player when it is in the meteor
func _process(delta):
	if playerInArea != null:
		playerInArea.damage(1)
	
func _physics_process(delta):
	rotation_degrees += rotationRate * delta
	
	position.y += speed * delta

# Meteor taking Damage
func damage(amount: int):
	if life <= 0:
		return
	
	life -= amount
	if life <= 0:
		# Meteor Effect when shot
		var effect = pMeteorEffect.instance()
		effect.position = position
		get_parent().add_child(effect)
		
		Signals.emit_signal("on_score_increment", 2)
		
		# Remove the Meteor from the tree when destroyed
		queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

# Keep damaging the Player when it is in the meteor
func _on_Meteor_area_entered(area):
	if area is Player:
		playerInArea = area

# Not damaging the Player when it is outside the meteor
func _on_Meteor_area_exited(area):
	if area is Player:
		playerInArea = null
