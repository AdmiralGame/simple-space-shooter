extends CPUParticles2D

func _ready():
	emitting = true

# Remove Meteor from the tree when done emitting	
func _process(delta):
	if !emitting:
		queue_free()
	
