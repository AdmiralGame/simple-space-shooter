extends Control

var pLifeIcon = preload("res://HUD/LifeIcon.tscn")

onready var lifeContainer := $LifeContainer
onready var scoreLabel := $Score

var score: int = 0

# Clear the lives whenever the HUD starts up
func _ready():
	clearLives()
#	setLives(6)
	Signals.connect("on_player_life_changed", self, "_on_player_life_changed")
	Signals.connect("on_score_increment", self, "_on_score_increment")
	
func clearLives():
	for child in lifeContainer.get_children():
		child.queue_free()

# Setting amount of lives in the container
func setLives(lives: int):
	clearLives()
	for i in range(lives):
		lifeContainer.add_child(pLifeIcon.instance())

# The function that will be called when the signal happens
func _on_player_life_changed(life: int):
	setLives(life)
	
# Increment Score
func _on_score_increment(amount: int):
	score += amount
	scoreLabel.text = str(score)
	
