extends CPUParticles2D

func _ready():
	emitting = true

# Remove Explosion Effect from the tree when done emitting
func _process(delta):
	if !emitting:
		queue_free()
	
	
